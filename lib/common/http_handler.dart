import 'dart:async';
import 'package:http/http.dart' as http;

class HttpHandler
{
  final baseUrl = "test.mindplus.io";
  Map<String, String> requestHeaders = {
       'Authorization': 'efb8ed201f6f7a2fcd6ed090f08094af',
       'Content-Type': 'application/x-www-form-urlencoded',
     };
  Future<dynamic> getResponse(uri, String type, {Map params}) async
  {
    http.Response response;
    if(type == "GET")
    {
      response = await http.get(uri, headers: requestHeaders);
    }
    else
    {
      response = await http.post(uri, body: params, headers: requestHeaders);
    }

     return response.body;
  }

  Future<dynamic> getWidget(String name, String parameters)
    {
      var uri = new Uri.https(baseUrl, '/v1/userLogin');

      Map<String, String> params = 
      {
        'name': name,
        'params': parameters
      };

      return getResponse(uri, 'POST', params: params).then(((data) => data));
    }
}